//
//  AppDelegate.h
//  银宝商城
//
//  Created by 屠赫宸 on 16/8/22.
//  Copyright © 2016年 屠赫宸. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

