//
//  AppDelegate.m
//  银宝商城
//
//  Created by 屠赫宸 on 16/8/22.
//  Copyright © 2016年 屠赫宸. All rights reserved.
//

#import "AppDelegate.h"
#import "MineViewController.h"
#import "ShopViewController.h"
#import "FindViewController.h"
#import "CartViewController.h"
#import "InfereViewController.h"
@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window=[[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor=[UIColor whiteColor];
    [self.window makeKeyAndVisible];
    [self createRootViewController];
    return YES;
}
-(void)createRootViewController
{
    
    UITabBarController *tabBC=[[UITabBarController alloc]init];
    
    MineViewController *mineVC=[[MineViewController alloc]init];
    UINavigationController *mineNC=[[UINavigationController alloc]initWithRootViewController:mineVC];
    mineNC.tabBarItem.title=@"我";
    
    ShopViewController *shopVC=[[ShopViewController alloc]init];
    UINavigationController *shopNC=[[UINavigationController alloc]initWithRootViewController:shopVC];
    shopNC.tabBarItem.title=@"商城";
    
    FindViewController *findVC=[[FindViewController alloc]init];
    UINavigationController *findNC=[[UINavigationController alloc]initWithRootViewController:findVC];
    findNC.tabBarItem.title=@"发现";
    
    CartViewController *cartVC=[[CartViewController alloc]init];
    UINavigationController *cartNC=[[UINavigationController alloc]initWithRootViewController:cartVC];
    cartNC.tabBarItem.title=@"购物车";
    
    tabBC.viewControllers=@[mineNC,shopNC,findNC,cartNC];
    [self.window setRootViewController:[InfereViewController new]];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
