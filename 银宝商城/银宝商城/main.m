//
//  main.m
//  银宝商城
//
//  Created by 屠赫宸 on 16/8/22.
//  Copyright © 2016年 屠赫宸. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
